﻿using UnityEngine;
using UnityEngine.XR;
using System.Collections;
using System.Collections.Generic;

public class HandPresence : MonoBehaviour {

    public bool showController = false;
    public InputDeviceCharacteristics controllerCharacteristics;
    public List<GameObject> controllerPrefabs;
    public GameObject handModelPrefab;

    InputDevice targetDevice;
    GameObject spawnnedController;
    GameObject spawnnedHandModel;
    Animator handAnimator;

    // Start is called before the first frame update
    void Start() {
        TryInitialize();
    }

    void TryInitialize() {
        List<InputDevice> devices = new List<InputDevice>();
        InputDevices.GetDevicesWithCharacteristics(controllerCharacteristics, devices);

        foreach (InputDevice item in devices) {
            Debug.Log(item.name + item.characteristics);
        }

        if (devices.Count > 0) {
            targetDevice = devices[0];
            GameObject prefab = controllerPrefabs.Find(controller => controller.name == targetDevice.name);

            if (prefab) {
                spawnnedController = Instantiate(prefab, transform);
            } else {
                Debug.Log("Did not find corresponding controller model");
                spawnnedController = Instantiate(controllerPrefabs[0], transform);

                if (spawnnedController == null) {
                    Debug.Log("algo de errado nao esta certo");
                }
            }

            spawnnedHandModel = Instantiate(handModelPrefab, transform);
            handAnimator = spawnnedHandModel.GetComponent<Animator>();
        }

        spawnnedHandModel = Instantiate(handModelPrefab, transform);
    }

    void UpdateHandAnimation() {
        if(targetDevice.TryGetFeatureValue(CommonUsages.trigger, out float triggerValue)) {
            handAnimator.SetFloat("Trigger", triggerValue);
        } else {
            handAnimator.SetFloat("Trigger", 0);
        }

        if (targetDevice.TryGetFeatureValue(CommonUsages.grip, out float gripValue)) {
            handAnimator.SetFloat("Grip", gripValue);
        } else {
            handAnimator.SetFloat("Grip", 0);
        }
    }

    // Update is called once per frame
    void Update() {
        /*
        if(targetDevice.TryGetFeatureValue(CommonUsages.primaryButton, out bool primaryButtonValue) && primaryButtonValue) {
            Debug.Log("Pressing Primary Button");
        }

        if (targetDevice.TryGetFeatureValue(CommonUsages.trigger, out float triggerValue) && triggerValue>0.1f) {
            Debug.Log("Trigger pressed"+ triggerValue);
        }

        if (targetDevice.TryGetFeatureValue(CommonUsages.primary2DAxis, out Vector2 primary2DAxisValue) && primary2DAxisValue != Vector2.zero) {
            Debug.Log("Primary Touchpad"+primary2DAxisValue);
        }
        */

        if (targetDevice == null) {
            TryInitialize();
        } else {
            if (showController) {
                spawnnedHandModel.SetActive(false);
                spawnnedController.SetActive(true);
            } else {
                spawnnedHandModel.SetActive(true);
                spawnnedController.SetActive(false);
                UpdateHandAnimation();
            }
        }
    }
}